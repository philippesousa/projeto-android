package br.com.connexo.smartagua.br.com.connexo.smartagua.condominio;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import br.com.connexo.smartagua.br.com.connexo.smartagua.dao.Conexao;

public class CondominioDao {

    private Conexao conexao;
    private SQLiteDatabase banco;



    public CondominioDao(Context context){
        conexao = new Conexao(context);
        banco = conexao.getWritableDatabase();
    }

    public long inserir(Condominio condominio){
        ContentValues values = new ContentValues();
        values.put("nome", condominio.getNome());
        values.put("endeneco", condominio.getEndereco());
        values.put("numero", condominio.getNumero());
        values.put("complemento", condominio.getComplemento());
        values.put("cidade", condominio.getCidade());
        values.put("uf", condominio.getUf());
        values.put("cep", condominio.getCep());
        return banco.insert("condominio", "complemento", values);
    }



}
