package br.com.connexo.smartagua.br.com.connexo.smartagua.condominio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import br.com.connexo.smartagua.R;

public class CadastrarCondominio extends AppCompatActivity {

    private EditText id;
    private EditText nome;
    private EditText endereco;
    private EditText numero;
    private EditText complemento;
    private EditText cidade;
    private EditText uf;
    private EditText cep;
    private CondominioDao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_condominio);

        nome = findViewById(R.id.editNome);
        endereco = findViewById(R.id.editEndereco);
        numero = findViewById(R.id.editNumero);
        complemento = findViewById(R.id.editComplemento);
        cidade = findViewById(R.id.editCidade);
        uf = findViewById(R.id.editUf;
        cep = findViewById(R.id.editCep);
        dao = new CondominioDao(this);
    }

    public void salvar(View view){
        Condominio c = new Condominio();
        c.setNome(nome.getText().toString());
        c.setEndereco(endereco.getText().toString());
        c.setComplemento(complemento.getText().toString());
        c.setCidade(cidade.getText().toString());
        c.setUf(uf.getText().toString());
        c.setCep(cep.get);
        dao.inserir(c);
    }
}
