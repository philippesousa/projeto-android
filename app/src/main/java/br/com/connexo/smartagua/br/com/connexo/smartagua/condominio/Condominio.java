package br.com.connexo.smartagua.br.com.connexo.smartagua.condominio;

import java.io.Serializable;
import java.util.List;

import br.com.connexo.smartagua.br.com.connexo.smartagua.agua.Leitura;
import br.com.connexo.smartagua.br.com.connexo.smartagua.apartamento.Unidade;
import br.com.connexo.smartagua.br.com.connexo.smartagua.usuario.Usuario;

public class Condominio implements Serializable {

    private Integer id;
    private String nome;
    private String endereco;
    private String numero;
    private String complemento;
    private String cidade;
    private String uf;
    private Long cep;
    private String Cnpj;
    private List<Unidade> unidades;
    private Usuario sindico;
    private Leitura leituraInicial;



    public List<Unidade> getUnidades() {
        return unidades;
    }
//
//    public Unidade getUnidade(String numero) {
//        return unidades.listIterator().
//    }
    public Usuario getSindico() {
        return sindico;
    }

    public void setSindico(Usuario sindico) {
        this.sindico = sindico;
    }

    public String getCnpj() {
        return Cnpj;
    }

    public void setCnpj(String cnpj) {
        this.Cnpj = cnpj;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public Long getCep() {
        return cep;
    }

    public void setCep(Long cep) {
        this.cep = cep;
    }
}