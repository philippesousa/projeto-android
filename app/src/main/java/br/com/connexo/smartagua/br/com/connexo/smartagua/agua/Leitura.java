package br.com.connexo.smartagua.br.com.connexo.smartagua.agua;

import java.util.Date;
import java.util.List;

import br.com.connexo.smartagua.br.com.connexo.smartagua.condominio.Condominio;
import br.com.connexo.smartagua.br.com.connexo.smartagua.usuario.Usuario;

public class Leitura {
    private Integer Id;
    private Condominio condominio;
    private Date dataLeitura;
    private Usuario responsavel;
    private boolean leituraInicial; // true no momento do cadastramento do condomínio, false nas demais leituras
    private Leitura leituraAnterior;  // serve para cálculo do consumo sem precisar pesquisar mês anterior
    private List<Medida> medidas;
}
