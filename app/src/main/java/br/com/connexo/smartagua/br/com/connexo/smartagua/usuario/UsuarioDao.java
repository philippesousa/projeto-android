package br.com.connexo.smartagua.br.com.connexo.smartagua.usuario;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.connexo.smartagua.br.com.connexo.smartagua.condominio.Condominio;
import br.com.connexo.smartagua.br.com.connexo.smartagua.dao.Conexao;

public class UsuarioDao {


    private Conexao conexao;
    private SQLiteDatabase banco;

    public UsuarioDao(Context context){
        conexao = new Conexao(context);
        banco = conexao.getWritableDatabase();
    }

    public long inserir(Usuario usuario){
        ContentValues values = new ContentValues();
        values.put("nome", usuario.getNome());
        values.put("cpf", usuario.getCpf());
        values.put("cep", usuario.getCpf());
        values.put("psw", usuario.getPsw());
        values.put("login", usuario.getLogin());
        values.put("email", usuario.getEmail());
        return banco.insert("usaurio", null, values);
    }

    public List<Usuario> obterCondominos(){
        List<Usuario> usuarios = new ArrayList<>();
        Cursor cursor = banco.query("usuario", new String[]{"nome", "cpf"}, null, null, null, null, null);

        while(cursor.moveToNext()){
            Usuario user = new Usuario();
            user.setNome(cursor.getString(0));
            user.setCpf(cursor.getString(1));
            usuarios.add(user);

        }
        return usuarios;
    }
}
