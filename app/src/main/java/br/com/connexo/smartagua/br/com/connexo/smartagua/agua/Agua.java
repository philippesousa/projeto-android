package br.com.connexo.smartagua.br.com.connexo.smartagua.agua;

import java.io.Serializable;
import java.sql.Date;

import br.com.connexo.smartagua.br.com.connexo.smartagua.condominio.Condominio;

public class Agua implements Serializable {

    private Integer id;
    private Integer consumoMensal;
    private Condominio condominio;
    private Date dataLeitura;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getConsumoMensal() {
        return consumoMensal;
    }

    public void setConsumoMensal(Integer consumoMensal) {
        this.consumoMensal = consumoMensal;
    }

    public Condominio getCondominio() {
        return condominio;
    }

    public void setCondominio(Condominio condominio) {
        this.condominio = condominio;
    }

    public Date getDataLeitura() {
        return dataLeitura;
    }

    public void setDataLeitura(Date dataLeitura) {
        this.dataLeitura = dataLeitura;
    }
}
