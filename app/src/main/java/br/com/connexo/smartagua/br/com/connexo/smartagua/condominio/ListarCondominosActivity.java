package br.com.connexo.smartagua.br.com.connexo.smartagua.condominio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.connexo.smartagua.R;
import br.com.connexo.smartagua.br.com.connexo.smartagua.usuario.Usuario;
import br.com.connexo.smartagua.br.com.connexo.smartagua.usuario.UsuarioDao;

public class ListarCondominosActivity extends AppCompatActivity {

    private ListView listView;
    private UsuarioDao dao;
    private List<Usuario> usuarios;
    private List<Usuario> usuariosFiltrados = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_condominos);

        listView = findViewById(R.id.listaCondominos);
        dao = new UsuarioDao(this);
        usuarios = dao.obterCondominos();
        usuariosFiltrados.addAll(usuarios);

        ArrayAdapter<Usuario> adapter = new ArrayAdapter<Usuario>(this, android.R.layout.simple_list_item_1, usuarios);
        listView.setAdapter(adapter);
    }
}
