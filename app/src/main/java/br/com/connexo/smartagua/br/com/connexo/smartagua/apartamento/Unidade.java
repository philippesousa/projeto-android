package br.com.connexo.smartagua.br.com.connexo.smartagua.apartamento;

import java.io.Serializable;

import br.com.connexo.smartagua.br.com.connexo.smartagua.condominio.Condominio;
import br.com.connexo.smartagua.br.com.connexo.smartagua.usuario.Usuario;

public class Unidade implements Serializable {
    private Integer id;
    private Condominio condominio;
    private String numero;
    private Usuario usuario;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Condominio getCondominio() {
        return condominio;
    }

    public void setCondominio(Condominio condominio) {
        this.condominio = condominio;
    }
}
