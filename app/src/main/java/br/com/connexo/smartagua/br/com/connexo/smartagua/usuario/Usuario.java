package br.com.connexo.smartagua.br.com.connexo.smartagua.usuario;

import java.io.Serializable;

import br.com.connexo.smartagua.br.com.connexo.smartagua.apartamento.Unidade;
import br.com.connexo.smartagua.br.com.connexo.smartagua.condominio.Condominio;

public class Usuario implements Serializable {

    private Integer id;
    private String nome;
    private String cpf;
    private String psw;
    private String login;
    private String email;
    private Unidade unidade;
    private Condominio condominio;

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public Condominio getCondominio() {
        return condominio;
    }

    public void setCondominio(Condominio condominio) {
        this.condominio = condominio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
