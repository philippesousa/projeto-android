package br.com.connexo.smartagua.br.com.connexo.smartagua.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Conexao extends SQLiteOpenHelper {
   private static final String nome = "banco.db";
    private static final int versao = 1;


    public Conexao(Context context) {
        super(context, nome, null, versao);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table condominio (id Integer primary key autoincrement," +
                "endereco varchar(50), numero varchar2(50), complemento varchar2(60), cidade varchar2(60), uf varchar2(2), cep number(8))");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
