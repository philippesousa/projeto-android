package br.com.connexo.smartagua.br.com.connexo.smartagua.agua;

import java.util.Currency;

import br.com.connexo.smartagua.br.com.connexo.smartagua.apartamento.Unidade;

public class Medida {
    private Integer id;
    private Leitura leitura;
    private Unidade unidade;
    private Integer leituraAtual; // número lido no hidrômetro
    private Integer leituraAnterior; // buscado na leitura anterior
    private Integer consumo; // leituraAtual - leituraAnterior (calculado)
    private Currency valorAgua; // calculado pela classe calculadora
    private Currency valorEsgoto; // igual ao da água
    private Currency valorTarifa;  // soma da água e esgoto

    public setValorAgua(Currency valor) {
        valorAgua = valor;
        valorEsgoto = valorAgua;
        valorTarifa = valorAgua + valorEsgoto;
    }

    public Currency getValorAgua() {
        return valorAgua;
    }

    public Currency getValorEsgoto() {
        return valorEsgoto;
    }

    public Currency getValorTarifa() {
        return valorTarifa;
    }

}
