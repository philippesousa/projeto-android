package br.com.connexo.smartagua.br.com.connexo.smartagua.agua;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import br.com.connexo.smartagua.br.com.connexo.smartagua.dao.Conexao;

public class AguaDao {

    private Conexao conexao;
    private SQLiteDatabase banco;

    public AguaDao(Context context){
        conexao = new Conexao(context);
        banco = conexao.getWritableDatabase();
    }

    public long inserir(Agua agua){
        ContentValues values = new ContentValues();
        values.put("consumoMensal", agua.getConsumoMensal());
        values.put("condominio", agua.getCondominio().getCnpj());
        values.put("dataLeitura", agua.getDataLeitura().toString());
        return banco.insert("agua", null, values);
    }


}
